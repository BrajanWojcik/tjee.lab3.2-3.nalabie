<%-- 
    Document   : newjsp
    Created on : 2022-05-27, 13:51:53
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Zatrudnieni pracownicy:</h1>
        <%
            String numer=request.getParameter("number");%>
        <c:import url="ludzie.xml" var="baza" />
        <x:parse doc="${baza}" var="wynik"/>
        <c:set var="numer" value="<%=numer%>"/>
        <ol>
        <x:forEach select="$wynik/ludzie/czlowiek[@id=$numer]" var="zasob">
            <li><b><x:out select="nazwisko" /></b>
            <x:out select="imie" /></li>
        </x:forEach>
        </ol>
    </body>
    <form action="wylogowanie.html" method="POST">
            <input type="submit" value="Wyloguj" />
    </form>
</html>
