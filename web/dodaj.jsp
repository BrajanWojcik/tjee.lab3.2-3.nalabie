<%-- 
    Document   : dodanie
    Created on : 2022-05-27, 23:49:13
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Dodaj nowego pracownika</h1>
        <form>
            <p>Wprowadz ID:</p>
            <input type="text" name="numerek" />
            <p>Wprowadz nazwisko:</p>
            <input type="text" name="naz" />
            <p>Wprowadz imie:</p>
            <input type="text" name="im" />
            <p></p>
            <input type="submit" value="Dodaj" name="confirmButton"/> 
            <a class="button rounded-full-btn reload-btn s-2 margin-bottom" href="index.html"><i class='icon-sli-arrow-left'></i>Powrót</a>
        </form>
    </body>
</html>
